import UIKit

class PushSegue: UIStoryboardSegue {
    
    override func perform() {
        // Assign the source and destination views to local variables.
        let containerViewController = self.source as! ContainerViewController
        let firstVCView = self.source.childViewControllers[0].view as UIView!
        let secondVCView = self.destination.view as UIView!
        
        // Get the screen width and height.
        let screenHeight = UIScreen.main.bounds.size.height
        let screenWidth = UIScreen.main.bounds.size.width
        // Specify the initial position of the destination view.
        secondVCView!.frame = CGRect.init(x:screenWidth, y:screenHeight-firstVCView!.frame.size.height, width:firstVCView!.frame.size.width, height:firstVCView!.frame.size.height)
        
        // Access the app's key window and insert the destination view above the current (source) one.
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(secondVCView!, aboveSubview: firstVCView!)
        
        // Animate the transition.
        UIView.animate(withDuration: TimeInterval(Constants.kAnimation), animations: { () -> Void in
            firstVCView!.frame = firstVCView!.frame.offsetBy(dx: -200, dy: 0)
            secondVCView!.frame = secondVCView!.frame.offsetBy(dx: -screenWidth, dy: 0)
            
        }) { (Finished) -> Void in
           containerViewController.swapFromViewController(starting: self.source.childViewControllers[0], ending: self.destination)
        }
        
    }

}

