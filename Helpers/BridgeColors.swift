import UIKit


extension UIColor {
    

    @nonobjc class var smoke: UIColor {
        return UIColor(red: 145.0 / 255.0, green: 144.0 / 255.0, blue: 145.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var soft: UIColor {
        return UIColor(white: 250.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var cherryLight: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 72.0 / 255.0, blue: 64.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var cherry: UIColor {
        return UIColor(red: 213.0 / 255.0, green: 47.0 / 255.0, blue: 38.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var dark: UIColor {
        return UIColor(white: 26.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var success: UIColor {
        return UIColor(red: 73.0 / 255.0, green: 204.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var alert: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 204.0 / 255.0, blue: 12.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var error: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 58.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var info: UIColor {
        return UIColor(red: 0.0, green: 113.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var light: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var cherryEnd: UIColor {
        return UIColor(red: 189.0 / 255.0, green: 33.0 / 255.0, blue: 25.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var cherryStart: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 87.0 / 255.0, blue: 79.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var smokeLighter: UIColor {
        return UIColor(white: 241.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var smokeDark: UIColor {
        return UIColor(red: 118.0 / 255.0, green: 115.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var infoLight: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 248.0 / 255.0, blue: 254.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var smokeLight: UIColor {
        return UIColor(white: 217.0 / 255.0, alpha: 1.0)
    }

    
    @nonobjc class var lemonStart: UIColor {
        return UIColor(red: 237.0 / 255.0, green: 207.0 / 255.0, blue: 100.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lemonEnd: UIColor {
        return UIColor(red: 230.0 / 255.0, green: 180.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
}




// Sample text styles
extension UIView {
    func animateFlipShadow(duration:Double){
        var animations = [CABasicAnimation]()
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = self.layer.shadowOpacity
        animation.toValue = 0.0
        animation.duration = duration / 2
        animations.append(animation)
        
        let animation2 = CABasicAnimation(keyPath: "shadowOpacity")
        animation2.fromValue = 0.0
        animation2.toValue = self.layer.shadowOpacity
        animation2.beginTime = duration / 2
        animation2.duration = duration / 2
        animations.append(animation2)
        
        
        let animation3 = CABasicAnimation(keyPath: "transform.scale.x")
        animation3.fromValue = 1
        animation3.toValue = 0.0
        animation3.duration = duration / 2
        animations.append(animation3)
        
        let animation4 = CABasicAnimation(keyPath: "transform.scale.x")
        animation4.fromValue = 0.0
        animation4.toValue = 1
        animation4.beginTime = duration / 2
        animation4.duration = duration / 2
        animations.append(animation4)
        
        let group = CAAnimationGroup()
        group.duration = duration
        group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        group.animations = animations
        
        self.layer.add(group, forKey: nil)
    }
    func setCardEffectMiddle(){
        self.backgroundColor = .clear
        self.layer.cornerRadius = Constants.kCornerRadius
        self.clipsToBounds = true
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.dark.cgColor
        self.layer.shadowOpacity = 0.15
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowRadius = 4
        self.layer.masksToBounds = false
    }
    func setCardEffectMiddle(bgColor: UIColor){
        self.backgroundColor = bgColor
        self.layer.cornerRadius = Constants.kCornerRadius
        self.clipsToBounds = true
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.dark.cgColor
        self.layer.shadowOpacity = 0.15
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowRadius = 4
        self.layer.masksToBounds = false
    }
    
    func setCardEffectFar(){
        self.backgroundColor = .clear
        self.layer.cornerRadius = Constants.kCornerRadius
        self.clipsToBounds = true
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.dark.cgColor
        self.layer.shadowOpacity = 0.15
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        self.layer.masksToBounds = false
    }
    func setCardEffectClose(){
        self.backgroundColor = .clear
        self.layer.cornerRadius = Constants.kCornerRadius
        self.clipsToBounds = true
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.dark.cgColor
        self.layer.shadowOpacity = 0.15
        self.layer.shadowOffset = CGSize(width: 0, height: 8)
        self.layer.shadowRadius = 8
        self.layer.masksToBounds = false
    }

    func setCardEffectInner(){
        self.layer.cornerRadius = Constants.kCornerRadius
        self.clipsToBounds = true
        
    }
    func removeCardEffect(){
        self.layer.cornerRadius = 0
        self.clipsToBounds = true
        
    }
    func setCardEffectInnerTopCorners(){
        self.layer.cornerRadius = Constants.kCornerRadius
        self.clipsToBounds = true
        self.roundCorners([.topLeft, .topRight], radius: Constants.kCornerRadius)
        //self.superview?.setCardEffectMiddle()
        
    }
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = CFTimeInterval(Constants.kAnimationShort)
        pulse.fromValue = 1
        pulse.toValue = 1.1
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        self.layer.add(pulse, forKey: "pulse")
    }
}

extension UIFont {
    
    
    
}
