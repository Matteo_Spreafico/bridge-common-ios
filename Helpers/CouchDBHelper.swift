//
//  CouchDBHelper.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 3/23/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit
import CouchDBiOSiPratico
class CouchDBHelper: NSObject {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */

    /*oo.table.lockdate = Date()
    oo.table.lockowner = tableController?.currentEmployee?.name ?? ""
    _ = oo.table.saveTable()*/
    
    
    public static func openTable(order:KDOpenOrder){
        order.openOrder.table.lockdate = Date()
        order.openOrder.table.lockowner = CookeryManager.sharedInstance.currentUser?.fullName ?? ""
        _ = order.openOrder.table.saveTable()
    }
    public static func closeTable(order:KDOpenOrder){
        order.openOrder.table.lockdate = nil
        order.openOrder.table.lockowner = nil
        _ = order.openOrder.table.saveTable()
    }
    public static func saveOpenOrder(order:KDOpenOrder, items:[KDOpenOrderItem], status:CBOpenOrderItem.OrderStatus){
        for item in items{
            let currentItem = item.openOrderItem
            currentItem.datecancelled = nil
            currentItem.dateready = nil
            currentItem.cookname = CookeryManager.sharedInstance.currentUser?.fullName ?? ""
            switch status {
            case CBOpenOrderItem.OrderStatus.toPrepare:
                currentItem.datepreparation = nil
            case CBOpenOrderItem.OrderStatus.inPreparation:
                currentItem.datepreparation = Date()
            case CBOpenOrderItem.OrderStatus.ready:
                currentItem.dateready = Date()
            case CBOpenOrderItem.OrderStatus.cancelled:
                currentItem.datecancelled = Date()
            }
            
            if(currentItem._onMutate != nil){
                currentItem._onMutate!()
                //Dati utente
                currentItem.lastupdatedate = Date()
                
                _ = order.openOrder.saveOpenOrder()
            }
        }
    }
}
