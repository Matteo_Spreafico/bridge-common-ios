//
//  Constants.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 4/5/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

struct Constants {
    
    /// returns 48
    static let kTableViewCellStandardHeight:CGFloat = 48.0
    
    /// returns 24
    static let kTableViewCellSmallHeight:CGFloat = 32.0
    
    /// returns 16
    static let kCornerRadius:CGFloat = 16.0
    
    /// returns 0.4
    static let kAnimationLong:CGFloat = 0.4
    
    /// returns 0.3
    static let kAnimation:CGFloat = 0.3
    
    /// returns 0.2
    static let kAnimationShort:CGFloat = 0.2
    
    /// returns 32 for iPadPro and 16 for standard iPads
    static var  kTableSpacing:CGFloat {
        if( Device().iPadPro ){
            return 32.0
        }
        else{
            return 16.0
        }
        
        
    }
    
    
    
}
