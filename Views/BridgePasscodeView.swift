//
//  BridgePasscodeView.swift
//  iPraticoOauth
//
//  Created by Matteo Spreafico on 3/1/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

class BridgePasscodeView: UIView {

    private let passCodeDotsCount = 4
    private let viewsArray:[UIView] = [UIView]()
    private var origin:CGPoint = CGPoint.init(x: 0, y: 0)
    private var coordinates:[CGPoint] = [CGPoint]()
    private let bulletSize:CGFloat = 16
    
    var passCode:[Int] = [Int]() {
        willSet { 
            //print("WillSet aVar to \(newValue) from \(passCode)")
            
        } 
        didSet { 
            print("didSet aVar to \(passCode) from \(oldValue)")
            if (passCode.count > oldValue.count){
                addNewBullet()
            }
            else if (passCode.count < oldValue.count && passCode.count != 0){
                removeLatestBullet()
            }
            else{
                removeBullets()
            }
        }
    }

    override func awakeFromNib() {
        setup()
    }
    
    func setup() {
        //origin of all bullets
        origin = CGPoint.init(x: self.bounds.size.width-bulletSize, y: self.bounds.size.height/2)
        //
        let padding:CGFloat = 60.0;
        let multiplier = (self.bounds.size.width - padding)/CGFloat(passCodeDotsCount);
        for i in 0..<passCodeDotsCount{
            let dest = CGPoint.init(x:multiplier*CGFloat(i+1)+padding/4, y:origin.y)
                 coordinates.append(dest)
        }

        
    }
    
    private func createBullet() -> UIView{
        let bullet:UIView = UIView(frame: CGRect(origin: coordinates[passCode.count-1], size: CGSize(width:bulletSize, height:bulletSize)))
        bullet.center = CGPoint(x: coordinates[passCode.count-1].x, y: coordinates[passCode.count-1].y)
        bullet.cornerRadius = bulletSize/2
        bullet.backgroundColor = UIColor.black
        bullet.tag = passCode.count-1
        
        bullet.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.addSubview(bullet)
        return bullet
    }
    
    private func addNewBullet(){
        let newBullet = createBullet()
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.40),
                       initialSpringVelocity: CGFloat(3.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        newBullet.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
       
        
        /*newBullet.animation
        .makeBounds().animate(1.0)*/
        /*newBullet.animation.makeScale(bulletSize)
        .thenAfter(0.3)
        .makeOrigin(coordinates[newBullet.tag].x, coordinates[newBullet.tag].y).bounce
        .animate(1.0)*/
        /*.thenAfter(0.3)
        .makeOrigin(coordinates[newBullet.tag].x, coordinates[newBullet.tag].y)
        .animate(0.5)*/
        
        
    }

    private func removeLatestBullet(){
       
        if let view = self.viewWithTag(passCode.count){
            UIView.animate(withDuration: 0.6,
                           animations: {
                            view.transform = CGAffineTransform(scaleX: 0, y: 0)
            },
                           completion: { _ in
                            view.removeFromSuperview()
            })
           
            
        }
        
    }
    private func removeBullets(){
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }

    
}

extension UIView {
    func shakeView(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        
        self.layer.add(animation, forKey: "position")
    }
}
