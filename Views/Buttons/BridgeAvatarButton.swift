//
//  BridgeAvatarButton.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 3/26/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit


import CouchDBiOSiPratico
import CouchbaseLite
extension UIButton
{
    func copyStyle(copyFrom:UIButton){
        self.isHidden = copyFrom.isHidden
        self.backgroundColor = copyFrom.backgroundColor
        self.setTitle(copyFrom.title(for: .normal), for: .normal)
        self.setTitleColor(copyFrom.titleColor(for: .normal), for: .normal)
        self.setBackgroundImage(copyFrom.backgroundImage(for: .normal), for: .normal)
    }
    
    func makeAvatarButton(userId:String){
        if let employee = CBEmployee.getEmployeeFromId(db: CDBManager.sharedInstance.database!, id: userId){
            NSLog("AVATAR : \(employee.name)")
            Duration.startMeasurement("Avatar")
            self.setTitle("", for: .normal)
            self.setImage(nil, for: .normal)
            self.setTitleColor(UIColor.cherry, for: .normal)
            self.setBackgroundImage (nil,for: .normal)
            if let image = employee.photo{
                self.setTitle(" ", for: .normal)
                self.setImage(image, for: .normal)
                self.layer.borderWidth = 0.0
                self.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
                self.imageView?.layer.cornerRadius = (self.imageView?.bounds.size.width)!/2
                self.imageView?.contentMode = .scaleAspectFit
            }
            else{
                self.setTitle(employee.avatarReplacement, for: .normal)
                self.setTitleColor(UIColor.cherry, for: .normal)
                self.setBackgroundImage (UIImage.init(named: "avatarCherry"),for: .normal)
                self.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
                self.contentHorizontalAlignment = .center
            }
            self.clipsToBounds = true
            self.isHidden = false
            //self.setNeedsDisplay()
            Duration.stopMeasurement()
            
        }
    }
}
class BridgeAvatarButton: BridgeRoundedButton {
    public var hasImage:Bool = false
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = self.bounds.size.height/2
        self.clipsToBounds = true
        self.setTitleColor(UIColor.cherry, for: .normal)
        
        
    }
    override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle(title, for: state)
        if (!hasImage){
            self.layer.borderColor = UIColor.cherry.cgColor
            self.layer.borderWidth = 2.0
        }
    }
    
    override func setImage(_ image: UIImage?, for state: UIControlState) {
        super.setImage(image, for: state)
        if (hasImage){
            self.setTitle("", for: .normal)
            self.layer.borderColor = UIColor.clear.cgColor
            self.layer.borderWidth = 0
        }
    }
    
}
