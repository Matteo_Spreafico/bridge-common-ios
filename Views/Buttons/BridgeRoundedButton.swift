//
//  BridgeRoundedButton.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 3/8/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit
enum BridgeButtonType {
    case success
    case error
    case alert
    case info
    case cookery
}
class BridgeRoundedButton: UIButton {
    
    var type: BridgeButtonType = .cookery{
        willSet {
            //print("WillSet aVar to \(newValue) from \(aVar)")
            switch newValue {
            case .success:
                self.backgroundColor = UIColor.success
            case .error:
                self.backgroundColor = UIColor.error
            case .alert:
                self.backgroundColor = UIColor.alert
            case .info:
                self.backgroundColor = UIColor.info
            default:
                self.backgroundColor = UIColor.cherry
            }
        }
        didSet {
            //print("didSet aVar to \(aVar) from \(oldValue)")
        }
    }
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = self.bounds.size.height/2
        self.clipsToBounds = true
        self.setTitleColor(UIColor.white, for: .normal)

    }
 

}
