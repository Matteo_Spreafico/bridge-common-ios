//
//  BridgeSearchBar.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 5/11/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

protocol BridgeSearchBarDelegate {
    
    func bridgeSearchBarBackButtonTapped()
    func bridgeSearchBarDidStartEditing()
    func bridgeSearchBarPerformedSearch(term:String)
}

class BridgeSearchBar: NibView {
    
    var delegate:BridgeSearchBarDelegate?
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var micIcon: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    var animatedLayer = CAShapeLayer()
    var maskLayer = CAShapeLayer()
    
    @IBAction func backButtonTapped(_ sender: Any) {
        searchField.text = ""
        delegate?.bridgeSearchBarBackButtonTapped()
    }
    
    override func awakeFromNib() {
        self.backgroundColor = .smokeLighter
        searchField.borderStyle = .none
        searchField.addTarget(self, action: #selector(didStartEditing(_:)),for: .editingDidBegin)
        searchField.leftViewMode = .always
        searchField.attributedPlaceholder = NSAttributedString(string: "Scrivi o parla per effettuare una ricerca", attributes: [NSAttributedStringKey.foregroundColor : UIColor.smokeLight])
        UITextField.appearance().tintColor = UIColor.cherry
        backButton.setTitleColor(UIColor.cherry, for: .normal)
        
        animatedLayer = CAShapeLayer()
        maskLayer = CAShapeLayer()
        animatedLayer.backgroundColor = UIColor.smokeLight.cgColor
        maskLayer.backgroundColor = UIColor.smokeLighter.cgColor
        searchField.delegate = self
        
        
        self.layer.insertSublayer(animatedLayer, at: 0)
        //self.layer.insertSublayer(maskLayer, at: 1)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        animatedLayer.frame = micIcon.frame
        animatedLayer.cornerRadius = animatedLayer.frame.size.width/2
        
        
        let clipsLayer = CALayer()
        clipsLayer.backgroundColor = UIColor.black.cgColor
        clipsLayer.frame = CGRect(x:0,y: -60,width:  self.frame.size.width, height: self.frame.size.height+72)
        //self.mask = clipsLayer
        
        self.layer.shadowColor = UIColor.smokeDark.cgColor
        self.layer.shadowRadius = 4.0
        self.layer.shadowPath = UIBezierPath(rect: CGRect.init(x: 0, y: self.bounds.size.height/2, width: self.bounds.size.width, height: self.bounds.size.height/2)).cgPath
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width:0,height:4)
        
        //self.frame will be correct here
    }
    
    @objc func didStartEditing(_ textField: UITextField) {
        delegate?.bridgeSearchBarDidStartEditing()
    }
    
    func animateMicImage(){
        
        let duration = 2.5
        let fadeAnimation = CAKeyframeAnimation(keyPath: "opacity")
        fadeAnimation.duration = duration
        fadeAnimation.values = [1,0,0]
        fadeAnimation.keyTimes = [0,0.4,1]
        fadeAnimation.repeatCount = Float.greatestFiniteMagnitude
        animatedLayer.opacity = 0.0
        
        
        let pulsing = CABasicAnimation(keyPath: "transform.scale")
        pulsing.fromValue = NSValue.init(caTransform3D: CATransform3DMakeScale(0, 0, 1.00))
        pulsing.toValue = NSValue.init(caTransform3D: CATransform3DMakeScale(30.00, 30.00, 10.00))
        pulsing.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulsing.duration = duration
        pulsing.repeatCount = Float.greatestFiniteMagnitude
        
        animatedLayer.add(pulsing, forKey: "Pulsing")
        animatedLayer.add(fadeAnimation, forKey: "FadeAnimation")
        
    }
}

extension BridgeSearchBar: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        delegate?.bridgeSearchBarPerformedSearch(term: "")
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        delegate?.bridgeSearchBarPerformedSearch(term:  newString)
        
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder();
        delegate?.bridgeSearchBarPerformedSearch(term:  textField.text!)
        
        
        return true;
    }
}
