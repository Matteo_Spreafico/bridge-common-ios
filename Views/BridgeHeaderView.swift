//
//  bridgeHeaderView.swift
//  iPraticoOauth
//
//  Created by Matteo Spreafico on 2/28/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

class BridgeHeaderView: NibView {
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonRight: UIButton!
    @IBOutlet weak var buttonMiddle: UIButton!
    @IBOutlet weak var buttonLeft: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var separatorView: UIView!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override func awakeFromNib() {
        titleLabel.textColor = UIColor.dark
        subtitleLabel.textColor = UIColor.dark
        containerView.backgroundColor = UIColor.light
        buttonRight.isHidden = true
        buttonMiddle.isHidden = true
        buttonLeft.isHidden = true
        separatorView.backgroundColor = UIColor.smokeLighter
        
    }
}

