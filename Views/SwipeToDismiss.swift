//
//  SwipeToDismiss.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 4/9/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

protocol PropertyStoring {
    
    associatedtype T
    
    func getAssociatedObject(_ key: UnsafeRawPointer!, defaultValue: T) -> T
}

extension PropertyStoring {
    func getAssociatedObject(_ key: UnsafeRawPointer!, defaultValue: T) -> T {
        guard let value = objc_getAssociatedObject(self, key) as? T else {
            return defaultValue
        }
        return value
    }
}

enum SwipeToDismissState {
    case on
    case off
}
extension UIView : PropertyStoring {
    
    typealias T = SwipeToDismissState
    
    private struct CustomProperties {
        static var swipeToDismiss = SwipeToDismissState.off
    }
    var swipeToDismiss: SwipeToDismissState {
        get {
            return getAssociatedObject(&CustomProperties.swipeToDismiss, defaultValue: CustomProperties.swipeToDismiss)
        }
        set {
            if (newValue == .on){
                initSwipeableGesture()
            }
            return objc_setAssociatedObject(self, &CustomProperties.swipeToDismiss, newValue, .OBJC_ASSOCIATION_RETAIN)
            
        }
    }
    private func initSwipeableGesture(){
        /*let panGesture = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        self.addGestureRecognizer(panGesture)*/
    }
    private func slideViewVerticallyTo(_ y: CGFloat) {
        self.frame.origin = CGPoint(x: 0, y: y)
    }
  
    
}
