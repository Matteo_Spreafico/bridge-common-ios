//
//  BridgeNavBar.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 3/5/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

class BridgeNavBar: NibView {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var buttonMiddle: UIButton!
    @IBOutlet weak var buttonRight: UIButton!
    @IBOutlet weak var buttonLeft: UIButton!
    @IBOutlet weak var layoutButton: UIButton!
    
    @IBOutlet weak var segmentStatus: UISegmentedControl!
    @IBOutlet weak var labelStatus: UILabel!
    
    
    override func awakeFromNib() {
       setupAvatarButton()
        segmentStatus.tintColor = UIColor.cherry
        labelStatus.textColor = UIColor.cherry
        segmentStatus.setTitle("Tutti", forSegmentAt: 0)
         segmentStatus.setTitle("Attesa", forSegmentAt: 1)
         segmentStatus.setTitle("In carico", forSegmentAt: 2)
         segmentStatus.setTitle("Pronto", forSegmentAt: 3)
         segmentStatus.setTitle("Bloccato", forSegmentAt: 4)
        labelStatus.isHidden = true
        
    }
    
    public func setupAvatarButton(){
        //buttonRight.backgroundColor = UIColor.red
        /*buttonRight.setImage(CookeryManager.sharedInstance.currentUser?.image, for: .normal)
        buttonRight.imageView?.layer.cornerRadius = (buttonRight.imageView?.bounds.size.width)!/2
        */
        if let id = CookeryManager.sharedInstance.currentUser?.idDoc{
        buttonRight.makeAvatarButton(userId:id)
        }
        
    }
    public func showOnlyMenu(){
        
        hideAll()
        menuButton.isHidden = false

        buttonRight.isHidden = false
        setupAvatarButton()
    }
    public func showAll(){
        menuButton.isHidden = false
        buttonMiddle.isHidden = false
        buttonRight.isHidden = false
        
        //buttonLeft.isHidden = false
        buttonRight.isHidden = false
        layoutButton.isHidden = false
        segmentStatus.isHidden = false
        //labelStatus.isHidden = false

    }
    public func hideAll(){
        menuButton.isHidden = true
        buttonMiddle.isHidden = true
        buttonRight.isHidden = true
        layoutButton.isHidden = true
        buttonLeft.isHidden = true
        buttonRight.isHidden = true
        
        segmentStatus.isHidden = true
        labelStatus.isHidden = true

    }
    
    
}
