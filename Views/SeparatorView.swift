//
//  SeparatorView.swift
//  KitchenDisplay
//
//  Created by Matteo Spreafico on 4/30/18.
//  Copyright © 2018 iPratico. All rights reserved.
//

import UIKit

class SeparatorView: UIView {

    
    func setBackgroundColor (color:UIColor){
        
        self.backgroundColor = color
    }
 

}
